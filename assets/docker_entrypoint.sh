#!/bin/bash
set -e

if [ "$1" = 'app' ]; then
    shift
    if [ "$1" = 'start' ]; then
        exec /usr/bin/erl -pa /usr/lib/simple-todos/websrv/websrv/ebin -s websrv serve
    else
        echo "Unrecognized command: app $1"
        exit 1
    fi
fi

exec "$@"
