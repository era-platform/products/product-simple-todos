.PHONY: all build dev-build run

all: build

build:
	DEV_HOME= ../ci/era-ci build $$(basename $$PWD) $$(git rev-parse --abbrev-ref HEAD)

dev-build:
	DEV_HOME=$$(dirname $$PWD) ../ci/era-ci build $$(basename $$PWD) $$(git rev-parse --abbrev-ref HEAD)

run:
	docker run -it --rm --name $$(basename $$PWD) -p8181:8181 $$(cat product.config.json|jq -r .output.name)

exec:
	docker exec -it $$(basename $$PWD) bash

distclean:
	rm -rf dist
